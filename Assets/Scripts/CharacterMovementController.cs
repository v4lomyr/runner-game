﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementController : MonoBehaviour
{

    [Header("Move")]
    public float moveAccel; // mengatur akselerasi
    public float maxSpeed;  // mengatur kecepatan maksimal

    [Header("Jump")]
    public float jumpAccel; // mengatur kecepatan elevasi ketika melompat
    private bool isOnGround; // mengecek letak character
    public bool isJumping; // mengecek state character (lompat atau tidak)

    [Header("Ground Raycast")]
    public float groundRaycastDistance; // jarak dengan raycast
    public LayerMask groundLayerMask; // layer yang di gunakan

    private Rigidbody2D rb2d; // rigidbody
    private Animator anim; // Animation Controller (Animator)
    private CharacterSoundController sound; // AudioSource

    [Header("Scoring")]
    public ScoreController score; // score controller
    public float scoringRatio;    // rasio score / jarak
    private float lastPositionX;  // mengambil jarak terakhir
    
    [Header("GameOver")]
    public GameObject gameOverScreen;
    public float fallPositionY;

    [Header("Camera")]
    public CameraMovementController gameCamera;


    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sound = GetComponent<CharacterSoundController>();
    }

    void Update()
    {
        // membaca input untuk lompat
        if (Input.GetMouseButtonDown(0))
        {
            if (isOnGround)
            {
                isJumping = true; // perubahan state character dari berjalan ke meloncat
                sound.PlayJump(); // putar audio untuk aksi "jump"
            }
        }

        // transisi animasi
        anim.SetBool("isOnGround", isOnGround);

        // scoring
        int distancePassed = Mathf.FloorToInt(transform.position.x - lastPositionX);
        int scoreIncrement = Mathf.FloorToInt(distancePassed / scoringRatio);

        if (scoreIncrement > 0)
        {
            score.IncreaseCurrentScore(scoreIncrement);
            lastPositionX += distancePassed;
        }

        // game over
        if (transform.position.y < fallPositionY)
        {
            GameOver();
        }
    }

    private void FixedUpdate()
    {
        // menghitung vektor kecepatan
        Vector2 velocityVector = rb2d.velocity;

        if (isJumping)
        {
            velocityVector.y += jumpAccel;
            isJumping = false;
        }
        velocityVector.x = Mathf.Clamp(velocityVector.x + moveAccel * Time.deltaTime, 0.0f, maxSpeed);  // membatasi kecepatan di antara 0 dan maxSpeed
        rb2d.velocity = velocityVector;

        // raycast ground
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, groundRaycastDistance, groundLayerMask);
        if (hit)
        {
            if (!isOnGround && rb2d.velocity.y <= 0)
            {
                isOnGround = true;
            }
        }
        else
        {
            isOnGround = false;
        }
    }

    private void GameOver()
    {
        score.FinishScoring();
        gameCamera.enabled = false;
        gameOverScreen.SetActive(true);
        this.enabled = false;
    }

    // debug
    private void OnDrawGizmos()
    {
        Debug.DrawLine(transform.position, transform.position + (Vector3.down * groundRaycastDistance), Color.white);
    }
}
