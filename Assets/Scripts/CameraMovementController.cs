﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementController : MonoBehaviour
{
    [Header("Position")]
    public Transform player;
    public float horizontalOffset;

    void Update()
    {
        // mendeteksi lokasi object player setelah itu memindahkan kamera ke lokasi objek tersebut
        Vector3 newPosition = transform.position;
        newPosition.x = player.position.x + horizontalOffset; 
        transform.position = newPosition;
    }
}
